package com.l30n13.socketio.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.l30n13.socketio.R;
import com.l30n13.socketio.database.pref.AppPreference;
import com.l30n13.socketio.database.pref.PrefKey;
import com.l30n13.socketio.model.MessageFormat;

import java.util.List;

/**
 * Created by leon on 26/4/19
 */
public class MessageRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<MessageFormat> messageList;

    public MessageRecyclerAdapter(Context mContext, List<MessageFormat> messageList) {
        this.mContext = mContext;
        this.messageList = messageList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case 0:
                return new UserConnectedViewHolder(LayoutInflater.from(mContext).inflate(R.layout.user_connected, viewGroup, false));
            case 1:
                return new SelfViewHolder(LayoutInflater.from(mContext).inflate(R.layout.my_message, viewGroup, false));
            default:
                return new OthersViewHolder(LayoutInflater.from(mContext).inflate(R.layout.their_message, viewGroup, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        MessageFormat message = messageList.get(position);
        switch (viewHolder.getItemViewType()) {
            case 0:
                ((UserConnectedViewHolder) viewHolder).tvMessageText.setText(message.getUsername());
                break;
            case 1:
                ((SelfViewHolder) viewHolder).tvMessageText.setText(message.getMessage());
                break;
            default:
                ((OthersViewHolder) viewHolder).tv_name.setText(message.getUsername());
                ((OthersViewHolder) viewHolder).tvMessageText.setText(message.getMessage());
                break;
        }
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (TextUtils.isEmpty(messageList.get(position).getMessage())) {
            return 0;
        } else if (messageList.get(position).getUniqueId().equals(AppPreference.getInstance(mContext).getString(PrefKey.UNIQUE_ID))) {
            return 1;
        } else {
            return 2;
        }
    }

    private class SelfViewHolder extends RecyclerView.ViewHolder {
        private TextView tvMessageText;

        private SelfViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMessageText = itemView.findViewById(R.id.tv_message_body);
        }
    }

    private class OthersViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name, tvMessageText;

        private OthersViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            tvMessageText = itemView.findViewById(R.id.tv_message_body);
        }
    }

    private class UserConnectedViewHolder extends RecyclerView.ViewHolder {
        private TextView tvMessageText;

        private UserConnectedViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMessageText = itemView.findViewById(R.id.tv_message_body);
        }
    }
}
