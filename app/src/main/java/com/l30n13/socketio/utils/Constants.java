package com.l30n13.socketio.utils;

/**
 * Created by leon on 25/4/19
 */
public class Constants {
    public static final String BASE_URL = "https://young-coast-86979.herokuapp.com/";
    public static final String TAG = "SocketIO-log";
}
