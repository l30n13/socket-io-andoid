package com.l30n13.socketio.Application;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by leon on 25/4/19
 */
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }
}
