package com.l30n13.socketio.model;

import java.io.Serializable;

/**
 * Created by leon on 25/4/19
 */
public class MessageFormat implements Serializable {

    private int id;
    private String username;
    private String message;
    private String uniqueId;

    public MessageFormat(String uniqueId, String username, String message) {
        this.username = username;
        this.message = message;
        this.uniqueId = uniqueId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }
}
