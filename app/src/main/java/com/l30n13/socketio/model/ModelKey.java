package com.l30n13.socketio.model;

/**
 * Created by leon on 2019-04-27
 */
public class ModelKey {
    public static final String USER_NAME = "user_name";
    public static final String UNIQUE_ID = "unique_id";
    public static final String MESSAGE = "message";
}
