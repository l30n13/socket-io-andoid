package com.l30n13.socketio.database.realm;

import com.l30n13.socketio.model.MessageFormat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by leon on 25/4/19
 */
public class RealmDatabase {
    private static RealmDatabase realmDatabase;

    private Realm realm;

    private RealmDatabase() {
        realm = Realm.getDefaultInstance();
    }

    public static RealmDatabase getInstance() {
        if (realmDatabase == null) {
            realmDatabase = new RealmDatabase();
        }
        return realmDatabase;
    }

    public void addData(MessageFormat messageFormat) {
        realm.beginTransaction();
        RealmModel realmModel = realm.createObject(RealmModel.class);
        realmModel.setId(realm.where(RealmModel.class).max("id").intValue() + 1);
        realmModel.setUniqueId(messageFormat.getUniqueId());
        realmModel.setUserName(messageFormat.getUsername());
        realmModel.setMessage(messageFormat.getMessage());
        realm.commitTransaction();
    }

    public Map<String, Object> getData(int start, int end) {
        RealmResults<RealmModel> results = realm.where(RealmModel.class).sort("id", Sort.ASCENDING).findAll();
        Map<String, Object> map = new HashMap<>();

        List<MessageFormat> messageFormatList = new ArrayList<>();

        if (start == 0) {
            start = results.size();
            end = results.size() - end;
        } else {
            start = results.size() - start;
            end = results.size() - end;
        }
        if (end < 0) {
            end = 0;
            map.put("hasData", false);
        } else {
            map.put("hasData", true);
        }
        for (int i = start - 1; i >= end; i--) {
            MessageFormat messageFormat = new MessageFormat(results.get(i).getUniqueId(), results.get(i).getUserName(), results.get(i).getMessage());
            messageFormatList.add(messageFormat);
        }
        Collections.reverse(messageFormatList);
        map.put("data", messageFormatList);

        return map;
    }
}
