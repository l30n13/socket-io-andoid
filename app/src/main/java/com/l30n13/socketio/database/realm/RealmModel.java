package com.l30n13.socketio.database.realm;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Created by leon on 25/4/19
 */
public class RealmModel extends RealmObject implements Serializable {
    private int id;
    private String uniqueId;
    private String userName;
    private String message;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
