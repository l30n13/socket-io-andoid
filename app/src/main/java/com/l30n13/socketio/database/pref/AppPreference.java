package com.l30n13.socketio.database.pref;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by leon on 25/4/19
 */
public class AppPreference {
    private static Context mContext;
    private static SharedPreferences preferences;
    private static AppPreference appPref;
    private SharedPreferences.Editor editor;

    private AppPreference() {
        preferences = mContext.getSharedPreferences(PrefKey.APP_PREF_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public static AppPreference getInstance(Context context) {
        if (appPref == null) {
            mContext = context;
            appPref = new AppPreference();
        }
        return appPref;
    }

    public void setString(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public String getString(String key) {
        return preferences.getString(key, null);
    }

    public void setBoolean(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.commit();
    }

    public Boolean getBoolean(String key, boolean defaultValue) {
        return preferences.getBoolean(key, defaultValue);
    }

    public void setInteger(String key, int value) {
        editor.putInt(key, value);
        editor.commit();
    }

    public int getInteger(String key) {
        return preferences.getInt(key, -1);
    }
}
