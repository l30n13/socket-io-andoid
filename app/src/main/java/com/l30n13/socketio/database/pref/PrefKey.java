package com.l30n13.socketio.database.pref;

/**
 * Created by leon on 25/4/19
 */
public class PrefKey {
    public static final String APP_PREF_NAME = "socket_io_chat";

    public static final String IS_REGISTERED = "is_registered";
    public static final String ID = "id";
    public static final String USER_NAME = "userName";
    public static final String UNIQUE_ID = "unique_id";
}
