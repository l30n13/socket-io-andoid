package com.l30n13.socketio.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.l30n13.socketio.R;
import com.l30n13.socketio.adapter.MessageRecyclerAdapter;
import com.l30n13.socketio.database.pref.AppPreference;
import com.l30n13.socketio.database.pref.PrefKey;
import com.l30n13.socketio.database.realm.RealmDatabase;
import com.l30n13.socketio.model.MessageFormat;
import com.l30n13.socketio.model.ModelKey;
import com.l30n13.socketio.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by leon on 25/4/19
 */
public class MainActivity extends AppCompatActivity {
    private Context mContext;

    private String uniqueId, username;
    private EditText tv_message;
    private ImageButton btn_send;

    private Boolean hasConnection = false;

    private List<MessageFormat> messageFormatList = new ArrayList<>();
    private LinearLayoutManager mLayoutManager;
    private RecyclerView rvMessage;
    private SwipeRefreshLayout srl_swipe;
    private MessageRecyclerAdapter messageRecyclerAdapter;
    private boolean hasData = true;
    private int start = 0, end = 10;
    private boolean isUserScrolling = false, isListGoingUp = false;

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(Constants.TAG, "run: ");
                    Log.i(Constants.TAG, "run: " + args.length);
                    JSONObject data = (JSONObject) args[0];
                    String username, message, id;
                    try {
                        username = data.getString(ModelKey.USER_NAME);
                        message = data.getString(ModelKey.MESSAGE);
                        id = data.getString(ModelKey.UNIQUE_ID);

                        Log.i(Constants.TAG, "run: " + username + message + id);

                        MessageFormat format = new MessageFormat(id, username, message);
                        messageFormatList.add(format);
                        messageRecyclerAdapter.notifyDataSetChanged();
                        rvMessage.smoothScrollToPosition(messageFormatList.size() - 1);
                        addToDatabase(format);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener onNewUser = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int length = args.length;

                    if (length == 0) {
                        return;
                    }
                    Log.i(Constants.TAG, "run: ");
                    Log.i(Constants.TAG, "run: " + args.length);
                    String username, uniqueId;
                    try {
                        JSONObject data = (JSONObject) args[0];
                        username = data.getString(ModelKey.USER_NAME);
                        uniqueId = data.getString(ModelKey.UNIQUE_ID);
                        MessageFormat format = new MessageFormat(uniqueId, username, null);
                        if (!uniqueId.equals(AppPreference.getInstance(mContext).getString(PrefKey.UNIQUE_ID))) {
                            messageFormatList.add(format);
                            messageRecyclerAdapter.notifyDataSetChanged();
                            rvMessage.smoothScrollToPosition(messageFormatList.size() - 1);
                        }
                        Log.i(Constants.TAG, "run: " + username);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private boolean startTyping = false;
    private int time = 2;
    @SuppressLint("HandlerLeak")
    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Log.i(Constants.TAG, "handleMessage: typing stopped " + startTyping);
            if (time == 0) {
                setTitle("SocketIO");
                Log.i(Constants.TAG, "handleMessage: typing stopped time is " + time);
                startTyping = false;
                time = 2;
            }
        }
    };

    private Socket mSocket;

    {
        try {
            mSocket = IO.socket(Constants.BASE_URL);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initView();
        initVariable();
        initFunctionality();
        initListener();

        if (savedInstanceState != null) {
            hasConnection = savedInstanceState.getBoolean("hasConnection");
        }

        if (!hasConnection) {
            mSocket.connect();
            mSocket.on("connect user", onNewUser);
            mSocket.on("chat message", onNewMessage);

            JSONObject user = new JSONObject();
            try {
                user.put(ModelKey.USER_NAME, username + " Connected");
                user.put(ModelKey.UNIQUE_ID, uniqueId);
                mSocket.emit("connect user", user);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {

        }

        Log.i(Constants.TAG, "onCreate: " + hasConnection);
        hasConnection = true;
        Log.i(Constants.TAG, "onCreate: " + username + " " + "Connected");


    }

    private void initView() {
        setContentView(R.layout.activity_main);
        tv_message = findViewById(R.id.tv_message);
        btn_send = findViewById(R.id.btn_send);

        srl_swipe = findViewById(R.id.srl_swipe);
        rvMessage = findViewById(R.id.rv_message);
        mLayoutManager = new LinearLayoutManager(mContext);
        rvMessage.setLayoutManager(mLayoutManager);
    }

    private void initVariable() {
        mContext = MainActivity.this;

        if (!getIntent().hasExtra("registered")) {
            username = getIntent().getStringExtra(ModelKey.USER_NAME);
            uniqueId = UUID.randomUUID().toString();

            AppPreference.getInstance(mContext).setBoolean(PrefKey.IS_REGISTERED, true);
            AppPreference.getInstance(mContext).setString(PrefKey.USER_NAME, username);
            AppPreference.getInstance(mContext).setString(PrefKey.UNIQUE_ID, uniqueId);

            Log.i(Constants.TAG, "onCreate: " + uniqueId);
        } else {
            username = AppPreference.getInstance(mContext).getString(PrefKey.USER_NAME);
            uniqueId = AppPreference.getInstance(mContext).getString(PrefKey.UNIQUE_ID);
            loadPreviousMessage();
        }

        setTitle(username);
    }

    private void loadPreviousMessage() {
        Map<String, Object> map = RealmDatabase.getInstance().getData(start, end);
        messageFormatList = (List<MessageFormat>) map.get("data");
        hasData = (Boolean) map.get("hasData");
        rvMessage.smoothScrollToPosition(messageFormatList.size() - 1);
    }

    private void initFunctionality() {
        messageRecyclerAdapter = new MessageRecyclerAdapter(mContext, messageFormatList);
        rvMessage.setAdapter(messageRecyclerAdapter);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("hasConnection", hasConnection);
    }

    public void initListener() {
        srl_swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srl_swipe.setRefreshing(false);
                if (hasData) {
                    srl_swipe.setRefreshing(true);

                    start = end;
                    end = end + 10;

                    Map<String, Object> map = RealmDatabase.getInstance().getData(start, end);
                    hasData = (Boolean) map.get("hasData");

                    List<MessageFormat> messageFormats = (List<MessageFormat>) map.get("data");
                    for (int i = 0; i < messageFormats.size(); i++)
                        messageFormatList.add(i, messageFormats.get(i));
                    messageRecyclerAdapter.notifyItemRangeChanged(0, messageFormatList.size());
                    srl_swipe.setRefreshing(false);
                } else {
                    srl_swipe.setRefreshing(false);
                }
            }
        });
        /*rvMessage.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    isUserScrolling = true;
                    if (isListGoingUp) {
                        int pastVisibleItems = mLayoutManager.findFirstCompletelyVisibleItemPosition();
                        if (pastVisibleItems == 0) {
                            if (hasData) {
                                srl_swipe.setRefreshing(true);

                                start = end;
                                end = end + 10;

                                Map<String, Object> map = RealmDatabase.getInstance().getData(start, end);
                                hasData = (Boolean) map.get("hasData");

                                List<MessageFormat> messageFormats = (List<MessageFormat>) map.get("data");
                                for (int i = 0; i < messageFormats.size(); i++)
                                    messageFormatList.add(i, messageFormats.get(i));
                                messageRecyclerAdapter.notifyItemRangeChanged(0, messageFormatList.size());
                                srl_swipe.setRefreshing(false);
                            } else {
                                srl_swipe.setRefreshing(false);
                            }
                        }
                    }
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (isUserScrolling) {
                    if (dy > 0) {
                        //means user finger is moving up but the list is going down
                        isListGoingUp = false;
                    } else {
                        //means user finger is moving down but the list is going up
                        isListGoingUp = true;
                    }
                }
            }
        });*/

        tv_message.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    btn_send.setEnabled(true);
                } else {
                    btn_send.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(Constants.TAG, "sendMessage: ");
                String message = tv_message.getText().toString().trim();
                if (TextUtils.isEmpty(message)) {
                    Log.i(Constants.TAG, "sendMessage:2 ");
                    return;
                }
                tv_message.setText("");
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(ModelKey.MESSAGE, message);
                    jsonObject.put(ModelKey.USER_NAME, username);
                    jsonObject.put(ModelKey.UNIQUE_ID, uniqueId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.i(Constants.TAG, "sendMessage: 1" + mSocket.emit("chat message", jsonObject));
            }
        });
    }

    private void addToDatabase(MessageFormat messageFormat) {
        RealmDatabase.getInstance().addData(messageFormat);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (isFinishing()) {
            Log.i(Constants.TAG, "onDestroy: ");

            JSONObject userId = new JSONObject();
            try {
                userId.put(ModelKey.USER_NAME, username + " DisConnected");
                userId.put(ModelKey.UNIQUE_ID, uniqueId + " DisConnected");
                mSocket.emit("connect user", userId);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            mSocket.disconnect();
            mSocket.off("chat message", onNewMessage);
            mSocket.off("connect user", onNewUser);
            username = "";
            messageFormatList.clear();
            messageRecyclerAdapter.notifyDataSetChanged();
        } else {
            Log.i(Constants.TAG, "onDestroy: is rotating.....");
        }
    }
}
