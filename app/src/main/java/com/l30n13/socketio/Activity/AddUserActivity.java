package com.l30n13.socketio.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.l30n13.socketio.R;
import com.l30n13.socketio.database.pref.AppPreference;
import com.l30n13.socketio.database.pref.PrefKey;
import com.l30n13.socketio.model.ModelKey;
import com.l30n13.socketio.utils.Constants;

/**
 * Created by leon on 25/4/19
 */
public class AddUserActivity extends AppCompatActivity {

    private Context mContext;
    private Button setNickName;
    private EditText userNickName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);

        mContext = AddUserActivity.this;

        if (alreadyConnected()) {
            Intent intent = new Intent(AddUserActivity.this, MainActivity.class);
            intent.putExtra("registered", true);
            startActivity(intent);
            finish();
        }

        userNickName = findViewById(R.id.userNickName);
        setNickName = findViewById(R.id.setNickName);


        userNickName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    setNickName.setEnabled(true);
                    Log.i(Constants.TAG, "onTextChanged: ABLED");
                } else {
                    Log.i(Constants.TAG, "onTextChanged: DISABLED");
                    setNickName.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });


        setNickName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddUserActivity.this, MainActivity.class);
                intent.putExtra(ModelKey.USER_NAME, userNickName.getText().toString());
                startActivity(intent);
            }
        });
    }

    private boolean alreadyConnected() {
        return AppPreference.getInstance(mContext).getBoolean(PrefKey.IS_REGISTERED, false);
    }
}
